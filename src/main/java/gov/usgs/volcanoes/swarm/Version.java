package gov.usgs.volcanoes.swarm;

public final class Version {

    public static final String BUILD_TIME="2024-10-16T13:28:40Z";

    public static final String POM_VERSION="3.3.4";

	public static final String VERSION_STRING = "Version: " + POM_VERSION + " Built: " + BUILD_TIME;
}